package nl.snowmanxl.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import nl.snowmanxl.component.CalculationComponent;

public class Calculation {
   private static StringProperty input;
   private static StringProperty operator;
   private static DoubleProperty result;

    private static CalculationComponent calc = new CalculationComponent();

   public Calculation(){
       input = new SimpleStringProperty("");
       operator = new SimpleStringProperty("");
       result = new SimpleDoubleProperty(0.0);
   }

   public static void addToInput(String i){
       input.setValue(getInput()+i);
   }

    public static String getInput() {
        return input.get();
    }

    public static StringProperty inputProperty() {
        return input;
    }

    public static void setInput(String input) {
        Calculation.input.set(input);
    }

    public static String getOperator() {
        return operator.get();
    }

    public static StringProperty operatorProperty() {
        return operator;
    }

    public static void setOperator(String operator) {
       calc.doCalculationOperation();
       Calculation.operator.set(operator);
    }

    public static double getResult() {
        return result.get();
    }

    public static DoubleProperty resultProperty() {
        return result;
    }

    public static void setResult(double dresult) {
       System.out.println(dresult);
       result.set(dresult);
    }
}
