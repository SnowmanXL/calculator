package nl.snowmanxl.controller;

import javafx.fxml.FXML;
import nl.snowmanxl.component.CalculationComponent;
import nl.snowmanxl.model.Calculation;

import static nl.snowmanxl.Main.showPopup;

public class ButtonController {

    @FXML
    private void initialize() {
    }


    public void calculate() {
       new CalculationComponent().doCalculationOperation();
       Calculation.setOperator("");
    }

    private void addToInput(String value) {
        Calculation.addToInput(value);
    }

    private void setInput(String value) {
        Calculation.setInput(value);
    }

    private void setOperator(String value) {
        Calculation.setOperator(value);
    }

    @FXML
    private void clickButtonPlus() {
        setOperator("+");
    }

    @FXML
    private void clickButtonMinus() {
        setOperator("-");
    }

    @FXML
    private void clickButtonDivide() {
        setOperator("/");
    }

    @FXML
    private void clickButtonMultiply() {
        setOperator("*");
    }

    @FXML
    private void clickButton1() {
        addToInput("1");
    }

    @FXML
    private void clickButton2() {
        addToInput("2");
    }

    @FXML
    private void clickButton3() {
        addToInput("3");
    }

    @FXML
    private void clickButton4() {
        addToInput("4");
    }

    @FXML
    private void clickButton5() {
        addToInput("5");
    }

    @FXML
    private void clickButton6() {
        addToInput("6");
    }

    @FXML
    private void clickButton7() {
        addToInput("7");
    }

    @FXML
    private void clickButton8() {
        addToInput("8");
    }

    @FXML
    private void clickButton9() {
        addToInput("9");
    }

    @FXML
    private void clickButton0() {
        addToInput("0");
    }

    @FXML
    private void clickButtonDecimal() {
        addToInput(".");
    }

    @FXML
    private void clickButtonDelete() {
        String str = Calculation.getInput();
        if (str != null && str.length() > 0) {
            setInput(str.substring(0, str.length() - 1));
        }
    }

    @FXML
    private void clickButtonClear() {
        setInput("");
    }

    @FXML
    private void clickButtonReset() {
        Calculation.setInput("");
        Calculation.setResult(0.0);
        Calculation.setOperator("");
    }

    @FXML
    private void iFeelLucky(){
        showPopup();
    }

}
