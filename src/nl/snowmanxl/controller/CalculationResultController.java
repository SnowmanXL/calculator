package nl.snowmanxl.controller;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import nl.snowmanxl.Main;
import nl.snowmanxl.model.Calculation;


public class CalculationResultController {

    private static Calculation calculation = new Calculation();

    @FXML
    private Label resultLabel;

    @FXML
    private Label operatorLabel;

    @FXML
    private TextField inputField;

    private Main main;

    @FXML
    private void initialize() {
        inputField.textProperty().bind(calculation.inputProperty());
        resultLabel.textProperty().bind(Bindings.convert(calculation.resultProperty()));
        operatorLabel.textProperty().bind(calculation.operatorProperty());
    }


}
