package nl.snowmanxl.component;

import nl.snowmanxl.model.Calculation;

import java.util.Objects;

public class CalculationComponent {


    public void doCalculationOperation(){
        String input = Calculation.getInput();
        String operator = Calculation.getOperator();
        Double result = Calculation.getResult();

        //Checks if input is null
        //If input is null no operation is required as the default output value is applicable
        if(input == null || Objects.equals(input, "")){
            return;
        }

        //Check on valid input
        try {
            Double.parseDouble(input);
        } catch (NumberFormatException e){
            System.out.println(" No valid number provided! ");
        }

        //TODO: handle situations after doing a calculation
        System.out.println(result);
        if (result == 0.0){
            Calculation.setResult(Double.parseDouble(input));
            Calculation.setInput("");
            return;
        }

        //TODO: Remove this check from this method
        // Checks if operator is null
        // If operator is null no operation is required only the input value will be set as output value
        // Check on valid double input
        if(operator == null || Objects.equals(input, "")){
            // TODO: Add popup with message
            Calculation.setResult(Double.parseDouble(input));
            return;
        }

        switch (operator){
            case "*":
                Calculation.setResult(multiply(input,result));
                Calculation.setInput("");
                break;
            case "-":
                Calculation.setResult(minus(input,result));
                Calculation.setInput("");
                break;
            case "+":
                Calculation.setResult(plus(input,result));
                Calculation.setInput("");
                break;
            case "/":
                Calculation.setResult(divide(input,result));
                Calculation.setInput("");
                break;
        }

    }

    private double multiply(String input, double previousInput){
       return previousInput * Double.parseDouble(input);
    }

    private double divide(String input, double previousInput){
        // TODO: NaN check
        return previousInput / Double.parseDouble(input);
    }

    private double minus(String input, double previousInput){
        return previousInput - Double.parseDouble(input);
    }

    private double plus(String input, double previousInput){
        return previousInput + Double.parseDouble(input);
    }
}
